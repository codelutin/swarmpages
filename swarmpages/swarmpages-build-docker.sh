#!/bin/ash

set -e

source /usr/local/lib/utils.sh

needs CI_REGISTRY CI_REGISTRY_USER CI_REGISTRY_PASSWORD CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA CI_PROJECT_PATH CI_PROJECT_TITLE

IMAGE_NAME=$CI_REGISTRY_IMAGE:swarmpages-$CI_COMMIT_SHORT_SHA

cat << EOF > docker-entrypoint.sh
#!/bin/ash
rm -rv /swarmpages/$CI_PROJECT_PATH/$CI_COMMIT_BRANCH
mkdir -vp /swarmpages/$CI_PROJECT_PATH/$CI_COMMIT_BRANCH
cp -avT /usr/src/site /swarmpages/$CI_PROJECT_PATH/$CI_COMMIT_BRANCH
EOF

if [ "$SP_BUILD_MAVEN_SITE" = "true" ] && [ -n "$SP_ADDRESSES"]; then
	INDEX_NAME="$(get_index_name $CI_PROJECT_TITLE)"
	FIRST_DOMAIN="$(get_first_domain "$SP_ADDRESSES")"
	cat <<- EOF >> docker-entrypoint.sh
	sleep 30
	echo "Demande de scrape du site"
	wget --header "X-Index-Name: $INDEX_NAME" --header "X-Project-Name: $CI_PROJECT_TITLE" --header "X-Project-Url: $FIRST_DOMAIN" scraper:9000
	EOF
else
	cat <<- EOF >> docker-entrypoint.sh
	# Éviter des erreurs lorsqu'on déploie en mode global ('update paused due to failure or early termination of task tu6i1n7l3bv1udsg1uu4irbt6')
	sleep 5
	EOF
fi

chmod +x docker-entrypoint.sh

# 4xx/5xx codes from https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
cat << EOF > error-codes.txt
400 Bad Request
401 Unauthorized
402 Payment Required
403 Forbidden
404 Not Found
405 Method Not Allowed
406 Not Acceptable
407 Proxy Authentication Required
408 Request Timeout
409 Conflict
410 Gone
411 Length Required
412 Precondition Failed
413 Payload Too Large
414 URI Too Long
415 Unsupported Media Type
416 Range Not Satisfiable
417 Expectation Failed
418 I'm a teapot (RFC 2324, RFC 7168)
421 Misdirected Request
422 Unprocessable Content
423 Locked (WebDAV; RFC 4918)
424 Failed Dependency (WebDAV; RFC 4918)
425 Too Early (RFC 8470)
426 Upgrade Required
428 Precondition Required (RFC 6585)
429 Too Many Requests (RFC 6585)
431 Request Header Fields Too Large (RFC 6585)
451 Unavailable For Legal Reasons (RFC 7725)
500 Internal Server Error
501 Not Implemented
502 Bad Gateway
503 Service Unavailable
504 Gateway Timeout
505 HTTP Version Not Supported
506 Variant Also Negotiates (RFC 2295)
507 Insufficient Storage (WebDAV; RFC 4918)
508 Loop Detected (WebDAV; RFC 5842)
510 Not Extended (RFC 2774)
511 Network Authentication Required (RFC 6585)
EOF

cat << "EOF" > Dockerfile
FROM busybox:1.36-musl
COPY public /usr/src/site

# Add default error pages
COPY error-codes.txt /usr/src/error-codes.txt
RUN cat /usr/src/error-codes.txt | while read -r CODE DESCRIPTION; do \
      if [ ! -f "/usr/src/site/$CODE.html" ]; then \
        echo "<!DOCTYPE html><html><head><title>$CODE Error</title><style>html { color-scheme: light dark; } h1 { text-align: center; }</style></head><body><h1>$CODE $DESCRIPTION</h1></body></html>" > /usr/src/site/$CODE.html; \
      fi \
    done

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]
EOF

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME
docker logout $CI_REGISTRY
