<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://maven.apache.org/POM/4.0.0" xmlns:_="http://maven.apache.org/POM/4.0.0" version="1.0">

  <!-- Copie du document -->
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <!-- Mise à jour scm -->
  <xsl:template match='//_:scm'>
    <xsl:element name="scm">
      <connection>scm:git:git@gitlab.nuiton.org:<xsl:value-of select="$ci_project_path"/>.git</connection>
      <!-- <developerConnection>scm:git:git@gitlab.nuiton.org:nuiton/nuiton-j2r.git</developerConnection> -->
      <url>https://gitlab.nuiton.org/<xsl:value-of select="$ci_project_path"/></url>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
