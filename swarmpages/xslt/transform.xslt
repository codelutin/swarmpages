<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" xmlns:_="http://www.w3.org/1999/xhtml" xmlns:DEFAULT="http://www.w3.org/1999/xhtml" version="1.0">

  <xsl:output omit-xml-declaration="yes" indent="yes" method="html"/>

  <!-- Copie du document -->
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <!-- Retirer les anciens scripts de génération du menu déroulant -->
  <xsl:template match='//_:script[contains(@src, "pom-site.js")]'>
  </xsl:template>

  <!-- Retirer l'ancien CSS' -->
  <xsl:template match='//_:link[contains(@href, "pom-site.css")]'>
  </xsl:template>

  <!-- Mise à jour logo Code Lutin -->
  <xsl:template match='//_:img[contains(@src, "images/lutinorange-codelutin.png") or contains(@src, "img/logos/code-lutin.svg")]'>
    <xsl:element name="img">
      <xsl:attribute name="src">https://www.codelutin.com/img/logos/code-lutin.svg</xsl:attribute>
      <xsl:attribute name="alt">Logo Code Lutin</xsl:attribute>
      <xsl:attribute name="width">250</xsl:attribute>
    </xsl:element>
  </xsl:template>

  <!-- Mise à jour logo Maven -->
  <xsl:template match='//_:a[contains(@href, "maven.apache.org") and @class="builtBy"]'>
    <xsl:element name="a">
      <xsl:attribute name="href">https://maven.apache.org</xsl:attribute>
      <xsl:attribute name="title">Maven</xsl:attribute>
      <xsl:attribute name="class">builtBy</xsl:attribute>
      <img class="builtBy" alt="Maven" src="https://common.nuiton.org/dev/images/logos/maven-feather.png"></img>
    </xsl:element>
  </xsl:template>

  <!-- Retirer la recherche Google -->
  <xsl:template match='//_:form[@id="search-form"]'>
  </xsl:template>
  <xsl:template match='//_:script[contains(@src, "google") or contains(text(),google)]'>
  </xsl:template>
  <xsl:template match='/_:html/_:head/_:link[last()]'>
    <xsl:if test="not(contains(@href, 'pom-site.css'))"> <!-- Ne pas défaire le retrait de pom-site.css-->
      <xsl:copy-of select="."/>
    </xsl:if>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="href">/vendor/typesense/typesense-docsearch-css@0.4.0</xsl:attribute>
    </xsl:element>
    <xsl:element name="meta">
      <xsl:attribute name="name">docsearch:version_tag</xsl:attribute>
      <xsl:attribute name="content"><xsl:value-of select="$version"/></xsl:attribute>
    </xsl:element>
  </xsl:template>
  <xsl:template match='/_:html/_:body/_:footer'>
    <xsl:copy-of select="."/>
    <xsl:element name="script">
      <xsl:attribute name="src">/vendor/typesense/typesense-docsearch.js@3.4.0</xsl:attribute>
    </xsl:element>
    <xsl:element name="script">
      docsearch({
        container: '#searchbar',
        typesenseCollectionName: '<xsl:value-of select="$collection_name"/>',
        typesenseServerConfig: {
          nodes: [{
            host: 'typesense.cloud.codelutin.com',
            port: '443',
            protocol: 'https'
          }],
          apiKey: '<xsl:value-of select="$search_api_key"/>', // Use API Key with only Search permissions
        },
        typesenseSearchParameters: {
          filter_by: 'version_tag:=<xsl:value-of select="$version"/>'
        },
      });
    </xsl:element>
  </xsl:template>

  <!-- Insérer le menu déroulant de choix de versions et la barre de recherche Typesense -->
  <xsl:template match='//_:ul[@class="breadcrumb"]/_:li[position()=1]'>
    <xsl:element name="li">
      <xsl:value-of select="$version_dropdown" disable-output-escaping="yes"/>
      <span class="divider">»</span>
    </xsl:element>
    <xsl:element name="li">
      <xsl:element name="div">
        <xsl:attribute name="id">searchbar</xsl:attribute>
        <xsl:attribute name="style">display: inherit;</xsl:attribute>
      </xsl:element>
      <span class="divider">»</span>
    </xsl:element>
    <xsl:copy-of select="."/>
  </xsl:template>

</xsl:stylesheet>
