grammar Address;

addresses: address (',' address)?;

address : (SCHEME '://')? rest;
rest: authority path?;
authority: host (':' PORT)?;
host: CHAR*;
path: ( '/' CHAR )+;

SCHEME: ('http'|'https');
PORT: ('80' | '443');
CHAR: [^?#*];
