#!/bin/ash

set -e

source /usr/local/lib/utils.sh
source /usr/local/lib/deploy.sh

generate_sp_stack_file > docker-compose.yml
TARGET=Swarmpages send_zip docker-compose.yml
