Include lib/deploy.sh
Include lib/utils.sh

# Comme warn mais n'écrit pas sur le stderr
testwarn() {
	echo -e "\033[1;33mWarning: $1\033[0m"
}

Describe 'Test generate_redir_labels()'
	It 'calls generate_redir_labels()'
		expected_output() { %text
			#|        caddy_1: old1.tld
			#|        caddy_1.import: prod_config
			#|        caddy_1.handle_path: /path1*
			#|        caddy_1.handle_path.redir: https://target.tld{uri}
			#|        caddy_2: http://old1.tld
			#|        caddy_2.import: prod_config
			#|        caddy_2.handle_path: /path2*
			#|        caddy_2.handle_path.redir: https://target.tld{uri}
			#|        caddy_3: old2.tld
			#|        caddy_3.import: prod_config
			#|        caddy_3.redir: https://target.tld{uri}
		}
		When call generate_redir_labels "https://target.tld" "old1.tld/path1, http://old1.tld/path2, old2.tld"
		The status should be success
		The output should equal "$(expected_output)"
	End
End

Describe 'Test generate_sp_stack_file()'
	setup() {
		CI_COMMIT_BRANCH=main
		CI_COMMIT_REF_NAME=main
		CI_DEFAULT_BRANCH=main
		CI_COMMIT_SHORT_SHA=d76c68be
		CI_PROJECT_PATH=mygroup/myproject
		CI_REGISTRY_IMAGE=registry.mycompany.tld/mygroup/myproject:v0.10.1
		SP_PROD_ADDRESSES=domain.tld
		SP_PROD_BRANCH=main
		SP_OLD_ADDRESSES="other.domain.tld, other2.domain2.tld"
	}
	Before setup

	It 'calls generate_sp_stack_file()'
		expected_output() { %text
			#|version: "3.7"
			#|
			#|volumes:
			#|  swarmpages:
			#|    external: true
			#|
			#|services:
			#|  swarmpages:
			#|    image: registry.mycompany.tld/mygroup/myproject:v0.10.1:swarmpages-d76c68be
			#|    volumes:
			#|      - swarmpages:/swarmpages
			#|    deploy:
			#|      mode: global
			#|      placement:
			#|        constraints:
			#|          - node.labels.region_name==FSN1
			#|      restart_policy:
			#|        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
			#|      labels:
			#|        caddy_0: "domain.tld"
			#|        caddy_0.import: prod_config
			#|        caddy_0.file_server:
			#|        caddy_0.root: "* /fileserver/swarmpages/mygroup/myproject/main"
			#|        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
			#|        caddy_0.handle_errors.file_server:
			#|        caddy_0.header_00: Cache-Control "public, max-age=2592000"
			#|        caddy_1: other.domain.tld
			#|        caddy_1.import: prod_config
			#|        caddy_1.redir: https://domain.tld{uri}
			#|        caddy_2: other2.domain2.tld
			#|        caddy_2.import: prod_config
			#|        caddy_2.redir: https://domain.tld{uri}
		}
		When run generate_sp_stack_file
		The status should be success
		The output should equal "$(expected_output)"
	End

	It 'calls generate_sp_stack_file() (empty SP_PROD_ADDRESSES)'
		unset SP_PROD_ADDRESSES
		expected_output() { %text
			#|version: "3.7"
			#|
			#|volumes:
			#|  swarmpages:
			#|    external: true
			#|
			#|services:
			#|  swarmpages:
			#|    image: registry.mycompany.tld/mygroup/myproject:v0.10.1:swarmpages-d76c68be
			#|    volumes:
			#|      - swarmpages:/swarmpages
			#|    deploy:
			#|      mode: global
			#|      placement:
			#|        constraints:
			#|          - node.labels.region_name==FSN1
			#|      restart_policy:
			#|        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
			#|      labels:
			#|        caddy_0: "main.myproject.mygroup.test.cloud.codelutin.com"
			#|        caddy_0.import: prod_config
			#|        caddy_0.file_server:
			#|        caddy_0.root: "* /fileserver/swarmpages/mygroup/myproject/main"
			#|        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
			#|        caddy_0.handle_errors.file_server:
			#|        caddy_0.header_00: Cache-Control "public, max-age=2592000"
		}
		When run generate_sp_stack_file
		The status should be success
		The output should equal "$(expected_output)"
	End

	It 'calls generate_sp_stack_file() (invalid address in SP_OLD_ADDRESSES)'
		SP_OLD_ADDRESSES="valid.example.com, invalid.example.com:8080"
		When run generate_sp_stack_file
		The status should be failure
		The stderr should include "invalid port: '8080'"
		The stdout should be present # Tmp, idéalement la fonction ne devrait rien afficher s'il y a une erreur
	End

	It 'calls generate_sp_stack_file() (custom SP_CACHE_CONTROL)'
		SP_CACHE_CONTROL="public, max-age=31536000"
		expected_output() { %text
			#|version: "3.7"
			#|
			#|volumes:
			#|  swarmpages:
			#|    external: true
			#|
			#|services:
			#|  swarmpages:
			#|    image: registry.mycompany.tld/mygroup/myproject:v0.10.1:swarmpages-d76c68be
			#|    volumes:
			#|      - swarmpages:/swarmpages
			#|    deploy:
			#|      mode: global
			#|      placement:
			#|        constraints:
			#|          - node.labels.region_name==FSN1
			#|      restart_policy:
			#|        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
			#|      labels:
			#|        caddy_0: "domain.tld"
			#|        caddy_0.import: prod_config
			#|        caddy_0.file_server:
			#|        caddy_0.root: "* /fileserver/swarmpages/mygroup/myproject/main"
			#|        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
			#|        caddy_0.handle_errors.file_server:
			#|        caddy_0.header_00: Cache-Control "public, max-age=31536000"
			#|        caddy_1: other.domain.tld
			#|        caddy_1.import: prod_config
			#|        caddy_1.redir: https://domain.tld{uri}
			#|        caddy_2: other2.domain2.tld
			#|        caddy_2.import: prod_config
			#|        caddy_2.redir: https://domain.tld{uri}
		}
		When run generate_sp_stack_file
		The status should be success
		The output should equal "$(expected_output)"
	End

	It 'calls generate_sp_stack_file() (legacy SP_ADDRESSES)'
		unset SP_PROD_ADDRESSES
		unset SP_PROD_BRANCH
		SP_ADDRESSES=domain.tld
		expected_output() { %text
			#|version: "3.7"
			#|
			#|volumes:
			#|  swarmpages:
			#|    external: true
			#|
			#|services:
			#|  swarmpages:
			#|    image: registry.mycompany.tld/mygroup/myproject:v0.10.1:swarmpages-d76c68be
			#|    volumes:
			#|      - swarmpages:/swarmpages
			#|    deploy:
			#|      mode: global
			#|      placement:
			#|        constraints:
			#|          - node.labels.region_name==FSN1
			#|      restart_policy:
			#|        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
			#|      labels:
			#|        caddy_0: "domain.tld"
			#|        caddy_0.import: prod_config
			#|        caddy_0.file_server:
			#|        caddy_0.root: "* /fileserver/swarmpages/mygroup/myproject/main"
			#|        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
			#|        caddy_0.handle_errors.file_server:
			#|        caddy_0.header_00: Cache-Control "public, max-age=2592000"
			#|        caddy_1: other.domain.tld
			#|        caddy_1.import: prod_config
			#|        caddy_1.redir: https://domain.tld{uri}
			#|        caddy_2: other2.domain2.tld
			#|        caddy_2.import: prod_config
			#|        caddy_2.redir: https://domain.tld{uri}
		}
		When run generate_sp_stack_file
		The status should be success
		The stderr should equal "$(testwarn "SP_ADDRESSES is deprecated")"
		The output should equal "$(expected_output)"
	End

	It 'calls generate_sp_stack_file() ($SP_BUILD_MAVEN_SITE=true)'
		SP_BUILD_MAVEN_SITE=true
		expected_output() { %text
			#|version: "3.7"
			#|
			#|networks:
			#|  scraper:
			#|    external: true
			#|
			#|volumes:
			#|  swarmpages:
			#|    external: true
			#|
			#|services:
			#|  swarmpages:
			#|    image: registry.mycompany.tld/mygroup/myproject:v0.10.1:swarmpages-d76c68be
			#|    networks: [scraper]
			#|    volumes:
			#|      - swarmpages:/swarmpages
			#|    deploy:
			#|      mode: global
			#|      placement:
			#|        constraints:
			#|          - node.labels.region_name==FSN1
			#|      restart_policy:
			#|        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
			#|      labels:
			#|        caddy_0: "domain.tld"
			#|        caddy_0.import: prod_config
			#|        caddy_0.file_server:
			#|        caddy_0.root: "* /fileserver/swarmpages/mygroup/myproject/main"
			#|        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
			#|        caddy_0.handle_errors.file_server:
			#|        caddy_0.header_00: Cache-Control "public, max-age=2592000"
			#|        caddy_0.header_10: /vendor/typesense/typesense-docsearch-css* content-type text/css
			#|        caddy_0.header_11: /vendor/typesense/typesense-docsearch.js* content-type text/javascript
			#|        caddy_1: other.domain.tld
			#|        caddy_1.import: prod_config
			#|        caddy_1.redir: https://domain.tld{uri}
			#|        caddy_2: other2.domain2.tld
			#|        caddy_2.import: prod_config
			#|        caddy_2.redir: https://domain.tld{uri}
		}
		When run generate_sp_stack_file
		The status should be success
		The output should equal "$(expected_output)"
	End
End

Describe 'Test add_templates()'
	setup(){
		TEST_DIR="$(mktemp -d)"
		cp "$SHELLSPEC_PROJECT_ROOT/test/yaml/add_templates_in.yml" "$TEST_DIR"
	}
	cleanup() {
		rm -r "$TEST_DIR"
	}

	Before setup
	After cleanup

	It 'calls add_templates()'
		export KEY=secret
		When run add_templates "$TEST_DIR/add_templates_in.yml"
		The status should be success
		The contents of file "$TEST_DIR/add_templates_in.yml" should equal "$(cat "$SHELLSPEC_PROJECT_ROOT/test/yaml/add_templates_out.yml")"
	End
End
