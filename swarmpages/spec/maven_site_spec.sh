Include lib/maven_site.sh
Include lib/utils.sh

Describe 'Test generate_version_dropdown()'
	It 'calls generate_version_dropdown()'
		expected_output() { %text
			#|<select id="versions" class="versions pull-left" onchange="location.pathname = this.value;">
			#|  <option value="v/v1.0.0">v1.0.0</option>
			#|  <option selected="selected" value="v/v1.0.1">v1.0.1</option>
			#|  <option value="v/v1.1.0">v1.1.0</option>
			#|</select>
		}
		When call generate_version_dropdown v1.0.1 v1.0.0 v1.0.1 v1.1.0
		The status should be success
		The output should equal "$(expected_output)"
	End
End

Describe 'Test generate_sites()'
	setup(){
		FAKE_TEST_DIR="$(mktemp -d)"
		CI_PROJECT_TITLE="Nuiton Utils"
		CI_PROJECT_PATH="/nuiton/nuiton-utils"
		SP_MAVEN_SITE_BRANCH="develop"
		SP_SEARCH_API_KEY="xyz"
		cp "$SHELLSPEC_PROJECT_ROOT/test/pom.xml" "$FAKE_TEST_DIR/pom.xml"
	}
	cleanup() {
		rm -r "$FAKE_TEST_DIR"
	}

	Before setup
	After cleanup

	It 'calls generate_sites()'
		git() {
			if [ "$1" == "tag" ]; then
				echo "v1.1.0 v1.0.1 v1.0.0"
			else
				return 0
			fi
		}
		mvn(){
			if [ "$VERSION" = "v1.0.0" ]; then
				# Simuler un échec du build mvn pour la v1.0.0
				return 1;
			fi
			mkdir -p "$FAKE_TEST_DIR/target/site"
			cp "$SHELLSPEC_PROJECT_ROOT/test/html/input_index.html" "$FAKE_TEST_DIR/target/site/index.html"
		}
		# Identique à la fonction dans utils.sh, mais n'écrit pas sur stderr, pour
		# éviter des problèmes avec Shellspec
		stdout_warn() {
			echo -e "\033[1;33mWarning: $1\033[0m"
		}
		expected_output() {
			info 'Starting mvn site generation for develop'
			%text
			#|renamed 'target/site' -> 'public/v/develop'
			info 'Starting mvn site generation for v1.1.0'
			%text
			#|renamed 'target/site' -> 'public/v/v1.1.0'
			info 'Starting mvn site generation for v1.0.1'
			%text
			#|renamed 'target/site' -> 'public/v/v1.0.1'
			info 'Starting mvn site generation for v1.0.0'
			info 'Transforming HTML for develop'
			info 'Transforming HTML for v1.1.0'
			info 'Transforming HTML for v1.0.1'
			%text
			#|'/usr/local/src/vendor' -> 'public/vendor'
		}

		expected_stderr_output() {
			stdout_warn "v1.0.0: 'mvn clean site -Preporting' failed"
			stdout_warn "v1.0.0: 'mvn clean site' failed (no index.html)"
			stdout_warn "Summary of errors:"
			stdout_warn "Version	Cause"
			stdout_warn "v1.0.0\t'mvn clean site -Preporting' failed"
			stdout_warn "v1.0.0\t'mvn clean site' failed (no index.html)"
		}

		When call generate_sites "$FAKE_TEST_DIR"
		The status should be success
		The output should equal "$(expected_output)"
		The stderr should equal "$(expected_stderr_output)"
		The contents of file "$FAKE_TEST_DIR/public/v/v1.0.1/index.html" should equal "$(cat $SHELLSPEC_PROJECT_ROOT/test/html/v1.0.1_expected_index.html)"
	End
End
