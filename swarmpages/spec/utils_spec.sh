Include lib/utils.sh

Describe 'Test get_first_domain()'
	It 'calls get_first_domain()'
		When call get_first_domain "domain1.tld, domain2.tld"
		The status should be success
		The output should equal "https://domain1.tld"
	End
	It 'calls get_first_domain() with protocol'
		When call get_first_domain "https://domain1.tld, domain2.tld"
		The status should be success
		The output should equal "https://domain1.tld"
	End
	It 'calls get_first_domain() with multiline'
		When call get_first_domain "$(echo -e "https://domain1.tld,\ndomain2.tld")"
		The status should be success
		The output should equal "https://domain1.tld"
	End
End

Describe 'Test get_index_name()'
	It 'calls get_index_name()'
		When call get_index_name "My%Project"
		The status should be success
		The output should equal "my-project"
	End
End
