#!/bin/ash

# Indenter ce fichier avec des tabs, sauf pour le Yaml suite aux 'cat <<- EOF',
# qui doit être indenté avec des espaces

# generate_redir_labels <TARGET> <ADDRESSES>
generate_redir_labels() {
	local TARGET="$1"
	local ADDRESSES="$2"

	local S="        " # Espaces pour l'indentation
	local i=1

	while read ADDRESS; do
		parse_address "$ADDRESS"

		echo "${S}caddy_$i: $ADDRESS_WITHOUT_PATH"
		echo "${S}caddy_$i.import: prod_config"
		if [ -n "$path" ]; then
			echo "${S}caddy_$i.handle_path: $path*"
			echo "${S}caddy_$i.handle_path.redir: $TARGET{uri}"
		else
			echo "${S}caddy_$i.redir: $TARGET{uri}"
		fi
		let i++
	done < <(echo $ADDRESSES | tr ',' '\n')
}

# generate_test_address
# stdout: an adress like branch.project.group.test.cloud.codelutin.com
generate_test_address(){
	needs CI_COMMIT_BRANCH CI_PROJECT_PATH

	echo -n "${CI_COMMIT_BRANCH}."
	echo "$CI_PROJECT_PATH" | tr '/' '\n' | tac | tr '\n' '.'
	echo test.cloud.codelutin.com
}

should_deploy_to_prod(){
	# TMP: support SP_ADDRESSES
	if [ -n "$SP_ADDRESSES" ] && [ -z "$SP_PROD_ADDRESSES" ]; then
		SP_PROD_ADDRESSES="$SP_ADDRESSES"
		SP_PROD_BRANCH="$CI_COMMIT_BRANCH"
	fi

	local SP_PROD_BRANCH="${SP_PROD_BRANCH:-$CI_DEFAULT_BRANCH}"

	[ -n "$SP_PROD_ADDRESSES" ] && ( [ "$SP_PROD_BRANCH" = "$CI_COMMIT_BRANCH" ] || [ -n "$CI_COMMIT_TAG" ] )
}

# generate_sp_stack_file
# stdout: stack file
generate_sp_stack_file() {
	needs CI_COMMIT_REF_NAME CI_COMMIT_SHORT_SHA CI_PROJECT_PATH CI_REGISTRY_IMAGE # TMP: support SP_ADDRESSES
	# needs CI_COMMIT_REF_NAME CI_COMMIT_SHORT_SHA CI_PROJECT_PATH CI_REGISTRY_IMAGE SP_PROD_ADDRESSES SP_PROD_BRANCH

	# TMP: support SP_ADDRESSES
	if [ -n "$SP_ADDRESSES" ]; then
		warn "SP_ADDRESSES is deprecated"
	fi

	local ADDRESSES="$(generate_test_address)"
	local SP_CACHE_CONTROL="${SP_CACHE_CONTROL:-public, max-age=2592000}"

	if should_deploy_to_prod; then
		ADDRESSES="$SP_PROD_ADDRESSES"
	fi

	validate_addresses "$ADDRESSES"

	local IMAGE_NAME=$CI_REGISTRY_IMAGE:swarmpages-$CI_COMMIT_SHORT_SHA
	cat <<- EOF
version: "3.7"

	EOF
	if [ "$SP_BUILD_MAVEN_SITE" = "true" ]; then
	cat <<- EOF
networks:
  scraper:
    external: true

	EOF
	fi
	cat <<- EOF
volumes:
  swarmpages:
    external: true

services:
  swarmpages:
    image: $IMAGE_NAME
	EOF
	if [ "$SP_BUILD_MAVEN_SITE" = "true" ]; then
		cat <<- EOF
    networks: [scraper]
		EOF
	fi
	cat <<- EOF
    volumes:
      - swarmpages:/swarmpages
    deploy:
      mode: global
      placement:
        constraints:
          - node.labels.region_name==FSN1
      restart_policy:
        condition: none # Ne pas relancer le conteneur une fois qu'il a fini
      labels:
        caddy_0: "$ADDRESSES"
        caddy_0.import: prod_config
        caddy_0.file_server:
        caddy_0.root: "* /fileserver/swarmpages/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME"
        caddy_0.handle_errors.rewrite: "* /{err.status_code}.html"
        caddy_0.handle_errors.file_server:
        caddy_0.header_00: Cache-Control "$SP_CACHE_CONTROL"
	EOF
	if [ -n "$HTAUTH_USER" ]; then
		needs HTAUTH_PASS
		cat <<- EOF
        caddy_0.basic_auth.$HTAUTH_USER: "$(caddy hash-password --plaintext "$HTAUTH_PASS" | sed 's#\$#$$#g')"
		EOF
	fi
	if [ "$SP_BUILD_MAVEN_SITE" = "true" ]; then
		# application/javascript est déprécié: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_Types#textjavascript
		cat <<- EOF
        caddy_0.header_10: /vendor/typesense/typesense-docsearch-css* content-type text/css
        caddy_0.header_11: /vendor/typesense/typesense-docsearch.js* content-type text/javascript
		EOF
	fi
	if [ -n "$SP_OLD_ADDRESSES" ] && should_deploy_to_prod; then
		validate_addresses "$SP_OLD_ADDRESSES"
		local FIRST_DOMAIN="$(get_first_domain "$ADDRESSES")"
		generate_redir_labels "$FIRST_DOMAIN" "$SP_OLD_ADDRESSES"
	fi
}

# add_templates <target_file>
add_templates() {
	local TARGET_FILE="$1"
	# '$$' deviendra in fine '$': '$$' --docker-stack-deploy--> '$'
	local USERNAME_ARG='--username=$${POSTGRES_USER:-postgres}'
	local DBNAME_ARG='--dbname=$${POSTGRES_DB:-postgres}'
	local STACK_FILE_WITH_TEMPLATES="x-templates:
  - &DEFAULT_PG14
    image: postgres:14-alpine
    networks:
      - default
      - barman
    environment:
      POSTGRES_PASSWORD: xxxxxxxxxx
    deploy:
      placement:
        constraints:
          - node.labels.region_name==FSN1
      resources:
        reservations:
          cpus: \"0.02\"
          memory: 50M
    healthcheck:
      test: pg_isready -q $USERNAME_ARG $DBNAME_ARG || exit 1
      interval: 10s
      timeout: 10s
      retries: 3
      start_period: 20m
  # Pour utiliser ce template, il faut définir un mdp lors de l'import, par
  # exemple:
  #
  #   etherpad-pg:
  #     <<: *DEFAULT_PG15
  #     environment:
  #       POSTGRES_PASSWORD: \$SOME_PASSWORD_FROM_CI_VARS
  #     volumes:
  #       - db:/var/lib/postgresql/data
  #
  - &DEFAULT_PG15
    image: postgres:15-alpine
    networks:
      - default
      - barman
    deploy:
      placement:
        constraints:
          - node.labels.region_name==FSN1
      resources:
        reservations:
          cpus: \"0.02\"
          memory: 50M
    healthcheck:
      test: pg_isready -q $USERNAME_ARG $DBNAME_ARG || exit 1
      interval: 10s
      timeout: 10s
      retries: 3
      start_period: 20m

$(envsubst < "$TARGET_FILE")
"

	echo "$STACK_FILE_WITH_TEMPLATES" > "$TARGET_FILE"
}

# send_zip <FILE_1> ... <FILE_N>
send_zip() {
	needs CI_COMMIT_MESSAGE CI_COMMIT_REF_SLUG CI_COMMIT_SHORT_SHA CI_DEFAULT_BRANCH CI_PROJECT_NAME SWARM_DEPLOYER_TOKEN TARGET

	local NAME="$(echo -n $CI_PROJECT_NAME | tr -c '[:alnum:]-_' '-')"
	local ZIP_NAME="$CI_PROJECT_NAME-${CI_COMMIT_REF_SLUG/$CI_DEFAULT_BRANCH/latest}.zip"
	local RESPONSE_FILE_1="$(mktemp)"
	local RESPONSE_FILE_2="$(mktemp)"

	zip -r "$ZIP_NAME" "$@"
	curl \
		--output "$RESPONSE_FILE_1" \
		-F "name=${NAME:0:63}" \
		-F "flavour=$CI_COMMIT_REF_SLUG" \
		-F "target=$TARGET" \
		-F "info=$CI_COMMIT_SHORT_SHA - ${CI_COMMIT_REF_SLUG/$CI_DEFAULT_BRANCH/latest} - $CI_COMMIT_MESSAGE" \
		-F "composeFile=docker-compose.yml" \
		-F "zip=@$ZIP_NAME" \
		"https://swarm-deployer.cloud.codelutin.com/api/v1/projects?swarm-deployer-token=$SWARM_DEPLOYER_TOKEN"

	# La réponse devrait ressembler à '{"id":"7903ca10-636b-474f-a7f8-57ea4ff46bdd"}'
	debug "Server response: $(cat "$RESPONSE_FILE_1")"

	# Vérifier qu'il n'y a pas d'erreur 4xx/5xx
	if [[ "$(jq '.StatusCode' "$RESPONSE_FILE_1")" =~ [4-5][0-9]{2} ]]; then
		err "Server returned error"
	fi

	# Récupérer l'id retourné et vérifier qu'il n'est pas nul
	local PROJECT_ID="$(jq --raw-output '.id' "$RESPONSE_FILE_1")"
	if [ "$PROJECT_ID" = null ]; then
		err "Null project ID"
	fi

	info "Zip sent successfully"

	if ! should_deploy_to_prod; then
		info "Trying to auto-deploy (works only with admin token)"
		curl \
			--output "$RESPONSE_FILE_2" \
			-X POST \
			"https://swarm-deployer.cloud.codelutin.com/api/v1/projects/$PROJECT_ID/deployments?swarm-deployer-token=$SWARM_DEPLOYER_TOKEN"

		debug "Server response: $(cat "$RESPONSE_FILE_2")"

		info "Site will be on $(generate_test_address)"
	fi
}
