#!/bin/ash

# err <msg>
err() {
	echo -e "\033[1;31mError: $1\033[0m" >&2
	exit 1
}

# warn <msg>
warn() {
	echo -e "\033[1;33mWarning: $1\033[0m" >&2
}

# info <msg>
info() {
	echo -e "\033[1;32mInfo: $1\033[0m"
}

# debug <msg>
debug() {
	echo -e "\033[0;34mDebug: $1\033[0m"
}
# parse_address <ADDRESS>
# sets ADDRESS_WITHOUT_PATH path
# Devrait être conforme avec swarmpages/grammar/Address.g4
parse_address() {
	# Effacer les espaces au début et à la fin
	local ADDRESS="$1"
	local SCHEME REST AUTHORITY HOST PORT
	unset ADDRESS_WITHOUT_PATH path

	if [[ "$ADDRESS" =~ "://" ]]; then
		SCHEME=$(echo $ADDRESS | awk -F	"://" '{print $1}')
		[[ "$SCHEME" =~ https? ]] || err "invalid scheme: '$SCHEME'"
		ADDRESS_WITHOUT_PATH="$SCHEME://"
	fi
	REST=${ADDRESS##$SCHEME://}

	IFS="/" read AUTHORITY path < <(echo $REST)
	[ -n "$path" ] && path="/$path"
	[[ "$path" =~ [?#*] ]] && err "invalid path: $path"

	IFS=":" read HOST PORT < <(echo $AUTHORITY)
	[[ "$HOST" =~ [?#*] ]] && err "invalid host: $HOST"
	[ "$PORT" = "80" -o "$PORT" = "443" -o -z "$PORT" ] || err "invalid port: '$PORT'"

	ADDRESS_WITHOUT_PATH="$ADDRESS_WITHOUT_PATH$AUTHORITY"
}

# validate_addresses <ADDRESSES>
# ADDRESSES is a comma-separated list of addresses (may be a single address)
validate_addresses() {
	local ADDRESSES="$1"

	while read ADDRESS; do
		parse_address "$ADDRESS"
	done < <(echo $ADDRESSES | tr ',' '\n')
}

# get_first_domain <SP_ADDRESSES>
# Retourne le premier domaine dans SP_ADDRESSES, précédé de https://
get_first_domain() {
	echo -n "https://"
	echo "$1" | tr -d '\n' | sed -E '/(https?:\/\/)?([^,]*).*/!d; s//\2/'
}

# get_index_name <CI_PROJECT_TITLE>
get_index_name() {
	echo -n "$1" | tr -c '[:alnum:]' '-' | tr '[:upper:]' '[:lower:]'
}

# Utils from wgslim-client (test here: https://gitlab.nuiton.org/codelutin/wgslim-client/-/blob/main/spec/utils_spec.sh)

# needs <var_1> ... <var_n>
needs() {
	local VAR_NAME
	for VAR_NAME in "$@"; do
		if expr match "$VAR_NAME" "[a-zA-Z0-9_]\+$" > /dev/null; then
			local VAR_CONTENTS="$(eval "echo \${$VAR_NAME}")"
			[ -z "$VAR_CONTENTS" ] && err "var \$$VAR_NAME is empty"
		else
			err "Invalid var name: $VAR_NAME"
		fi
	done
	return 0
}
