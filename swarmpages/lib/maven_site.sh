#!/bin/ash

# generate_version_dropdown <SELECTED_OPTION> <OPT_1> ... <OPT_N>
# Génère un menu dépliant du type:
#	 <select id="versions" class="versions pull-left" onchange="location = this.value;">
#		 <option value="v/opt_1">opt_1</option>
#		 <!-- (...) -->
#		 <option value="v/opt_n">opt_n</option>
#	 </select>
# Lorsqu'on tombe sur SELECTED_OPTION, on aura <option selected="selected" ...>
generate_version_dropdown() {
	local SELECTED_OPTION="$1"

	shift

	echo '<select id="versions" class="versions pull-left" onchange="location.pathname = this.value;">'
	for OPT in $@; do
		if [ "$OPT" = "$SELECTED_OPTION" ]; then
			echo "  <option selected=\"selected\" value=\"v/$OPT\">$OPT</option>"
		else
			echo "  <option value=\"v/$OPT\">$OPT</option>"
		fi
	done
	echo '</select>'
}

# record_error <MESSAGE>
record_error() {
	needs VERSION
	local MESSAGE="$1"

	VERSIONS_WITH_ERRORS="$VERSIONS_WITH_ERRORS$VERSION\t$MESSAGE\n"
	warn "$VERSION: $MESSAGE"
}

build_site() {
	needs CI_PROJECT_PATH VERSION

	info "Starting mvn site generation for $VERSION"

	if [ -s "public/v/$VERSION/index.html" ]; then
		record_error "A site for $VERSION already exists, skipping..."
		SITES_TO_TRANSFORM="$SITES_TO_TRANSFORM $VERSION"
		return 1
	fi

	git checkout -f $VERSION

	# Mettre à jour la section <scm> du pom.xml
	# Refait marcher la génération lorsque le scm configuré est svn
	xsltproc \
		--output pom.xml \
		--stringparam ci_project_path "$CI_PROJECT_PATH" \
		/usr/local/src/update_scm.xslt pom.xml || record_error "xsltproc transform failed for pom.xml"

	# On essaye de construire le site avec -Preporting. En cas d'échec, on essaye
	# sans -Preporting. En cas de nouvel échec, on retourne une erreur
	mvn clean site -Preporting
	if [ $? -ne 0 ]; then
		record_error "'mvn clean site -Preporting' failed"
		mvn clean site
		if [ $? -ne 0 ]; then
			# Même lorsque 'mvn clean site' échoue, on peut retrouver un site partiel, qui est mieux que rien
			if [ ! -s "target/site/index.html" ]; then
				record_error "'mvn clean site' failed (no index.html)"
				return 1
			else
				record_error "'mvn clean site' failed (but at least index.html exists)"
			fi
		fi
	fi

	mv -vT target/site public/v/$VERSION
	SITES_TO_TRANSFORM="$SITES_TO_TRANSFORM $VERSION"
}

# generate_site
# Retourne 1 s'il y a eu un soucis lors de la génération, 0 sinon
transform_site() {
	needs SP_SEARCH_API_KEY COLLECTION_NAME VERSION SITES_TO_TRANSFORM

	info "Transforming HTML for $VERSION"

	local VERSION_DROPDOWN="$(generate_version_dropdown $VERSION $SITES_TO_TRANSFORM)"

	# On transforme juste les fichiers à la racine du site. On ne touche pas les
	# éventuels dossiers 'apidocs', 'testapidocs', etc.
	for FILE in public/v/$VERSION/*.html; do

		# Nettoyer le html produit par mvn avec tidy. Si on ne le fait pas, on aura
		# des problèmes avec xsltproc.
		tidy -q -wrap 120 -modify -numeric --show-warnings no "$FILE"
		# return codes: 0 if everything is fine, 1 if there were warnings and 2 if there were errors
		[ "$?" -gt 1 ] && record_error "tidy error (first pass) for $FILE"

		# Quelques remplacements
		sed -i '
			s#http://www.codelutin.com#https://www.codelutin.com#g
			s#http://www.libre-entreprise.org#https://www.libre-entreprise.org#g
			s#http://maven.apache.org/#https://maven.apache.org/#g
			' "$FILE"

		# Transformations avec XSLT
		xsltproc \
			--output "$FILE" \
			--stringparam collection_name "$COLLECTION_NAME" \
			--stringparam search_api_key "$SP_SEARCH_API_KEY" \
			--stringparam version "$VERSION" \
			--stringparam version_dropdown "$VERSION_DROPDOWN" \
			/usr/local/src/transform.xslt "$FILE" || record_error "xsltproc transform failed for $FILE"

		# Nettoyer le html à nouveau (xsltproc ne produit pas un résultat correctement formaté)
		tidy -q -indent -ashtml -wrap 120 -modify -numeric --show-warnings no "$FILE"
		[ "$?" -gt 1 ] && record_error "tidy error (second pass) for file $FILE"

	done
}

# generate_sites <PROJECT_DIR>
generate_sites(){
	needs CI_PROJECT_TITLE SP_SEARCH_API_KEY SP_MAVEN_SITE_BRANCH

	local PROJECT_DIR="$1"

	[ -d "$PROJECT_DIR" ] && cd "$PROJECT_DIR" || err "Cannot cd to $PROJECT_DIR"

	# Tags ordonnées de la plus récente à la plus ancienne
	local TAGS="$(git tag --sort=-taggerdate)"
	local COLLECTION_NAME="$(get_index_name $CI_PROJECT_TITLE)"
	local VERSIONS="$SP_MAVEN_SITE_BRANCH $TAGS"
	local VERSION VERSIONS_WITH_ERRORS SITES_TO_TRANSFORM

	mkdir -p public/v

	for VERSION in $VERSIONS; do
		build_site
	done

	for VERSION in $SITES_TO_TRANSFORM; do
		transform_site
	done

	# Ajout index.html qui redirige vers $SP_MAVEN_SITE_BRANCH
	echo "<html><head><meta http-equiv=\"Refresh\" content=\"0; url='/v/$SP_MAVEN_SITE_BRANCH'\"></head><body></body></html>" > public/index.html

	cp -rv /usr/local/src/vendor public

	if [ -n "$VERSIONS_WITH_ERRORS" ]; then
		warn "Summary of errors:"
		warn "Version	Cause"
		while read LINE; do
			[ -n "$LINE" ] && warn "$LINE"
		done < <(echo -e "$VERSIONS_WITH_ERRORS")
	fi

	return 0
}
