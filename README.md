# Swarmpages

Permet de déployer des sites sur le swarm. Un exemple fonctionnel et minimal est disponible sur https://gitlab.nuiton.org/codelutin/admsys/demos/deploiement-swarmpages

## Mise en place simple sur un nouveau projet

* S'assurer d'avoir une variable `SWARM_DEPLOYER_TOKEN` au niveau des variables de la CI (elle peut être héritée sur certains groupes)
* L'utilisateur @swarm_bot doit être ajouté aux members du projet
* Importer le template `swarmpages.yml`:
  ```yml
  include:
  - project: codelutin/swarmpages
    file: swarmpages_base.yml
  ```
* Ajouter des stages `swarmpages-build` puis `swarmpages`
* Créer un job de build du site. Ce job doit être dans le stage `swarmpages-build` et mettre le site construit dans un dossier `public`, qui doit être mis en artifact. Par exemple:
  ```yml
  swarmpages-build:
    image: node:lts
    stage: swarmpages-build
    before_script:
      - npm ci --cache .npm --prefer-offline
    script:
      - npm run build
    artifacts:
      paths:
        - public
    cache:
      key: npm
      paths:
        - .npm
  ```
* Le template `swarmpages.yml` ajoutera deux jobs au stage `swarmpages` (lancés automatiquement):
  * `swarmpages-build-docker`: Build d'une image Docker contenant les sources du site
  * `swarmpages-deploy`: Génération d'un fichier `docker-compose.yml` et envoi vers swarm-deployer

Une stack sera générée et envoyée vers swarm-deployer. Un déploiement automatique sera tenté, mais pour le moment il ne fonctionne qu'avec un token admin. Si le déploiement ne fonctionne pas, il faudra le lancer à la main depuis https://swarm-deployer.cloud.codelutin.com.

Le site aura une URL de la forme `branche.projet.chemin.test.cloud.codelutin.com`.

### Déploiement en prod

On peut configurer le déploiement en prod avec les variables suivantes:
* `SP_PROD_ADDRESSES`: la ou les adresses de prod (pour le détail des formats supportés, voir [Adresses](#adresses))
* `SP_PROD_BRANCH`: branche à utiliser pour le déploiement en prod

Lors d'un commit sur `SP_PROD_BRANCH`, ou d'un tag, le déploiement sera manuel, via swarm-deployer. Le site sera disponible sur `SP_PROD_ADDRESSES`, et chacune des adresses dans `SP_OLD_ADDRESSES` redirigera vers la première adresse dans `SP_PROD_ADDRESSES`.

## Variables supportées

| Variable                       | Requis ? | Description                                                                                                                                                                                       |
| ------------------------------ | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `SP_PROD_ADDRESSES`            | Non      | Une ou plusieurs adresses depuis lesquelles le site sera accessible.                                                                                                                              |
| `SP_PROD_BRANCH`               | Non      | Branche à utiliser pour le déploiement en prod. Par défaut, même valeur que `CI_DEFAULT_BRANCH`                                                                                                   |
| `SP_ADDRESSES`                 | Non      | Dépréciée: utiliser plutôt `SP_PROD_ADDRESSES`                                                                                                                                                    |
| `SP_OLD_ADDRESSES`             | Non      | Une ou plusieurs adresses. Ajoute une redirection depuis ces adresses vers la première adresse dans `SP_PROD_ADDRESSES` (redirection en HTTPS, pour tous les sous-chemins, et préservation du chemin)  |
| `SWARM_DEPLOYER_TOKEN`         | Oui      | Token pour s'authentifier sur https://swarm-deployer.cloud.codelutin.com/ (en place au niveau du groupe `codelutin` sur gitlab.nuiton.org. Si votre projet est dedans, pas besoin de la rajouter) |
| `HTAUTH_USER`<br>`HTAUTH_PASS` | Non      | Pour mettre en place une authentification HTTP basique. Les deux variables doivent être définies en même temps                                                                                    |
| `SP_CACHE_CONTROL`             | Non      | Personnaliser la valeur du header Cache-Control (par défaut `public, max-age=2592000`)                                                                                                            |
| `SP_BUILD_MAVEN_SITE`          | Non      | `true` pour ajouter un job de build de sites Maven                                                                                                                                                |
| `SP_MAVEN_SITE_BRANCH`         | Oui*     | Branche à utiliser pour le build de sites Maven (\*requis lorsque `SP_BUILD_MAVEN_SITE=true`)                                                                                                     |

### Adresses

Les addresses sont similaires à des URLs, mais avec quelques différences:

* Le schéma est optionnel mais peut être seulement `http` ou `https`
* Le port est optionnel mais peut être seulement `80` ou `443`
* Pas de query ou de fragment
* L'adresse ne doit pas contenir de `*` (interprété comme un wildcard par Caddy)

Exemple:

```yml
variables:
  SP_PROD_ADDRESSES: example.com, www.example.com
  SP_OLD_ADDRESSES: example.page.yourgitlab.com/oldwebsite
```

La grammaire est définie de manière précise dans [swarmpages/grammar/Address.g4](swarmpages/grammar/Address.g4)

### Erreurs 404

Lorsqu'on demande un chemin qui n'existe pas:

* Si le fichier `404.html` existe à la racine du site, celui-ci sera renvoyé (il faut lucaslorentz/caddy-docker-proxy:2.8 ou plus récent)
* Sinon, Caddy répondra avec un code `404` et pas de contenu

## Build de sites Maven

Lorsqu'on l'active, un job `swarmpages-build-maven-site` est ajouté au stage `swarmpages-build`

* Un site maven est généré pour chaque tag du dépôt, ainsi que pour `SP_MAVEN_SITE_BRANCH` (par défaut `$CI_DEFAULT_BRANCH`)
* Un menu dépliant avec les choix de versions est généré et inséré dans chaque site
* Les sites sont stockés dans `public/v/<version>`. Le dossier `public` est envoyé en artifact
* Un `index.html` à la racine de `public` redirige vers la dernière version (ToDo)
