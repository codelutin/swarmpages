FROM caddy:2.8.4

# Label pour que l'image ne soit pas effacée sur glr2
LABEL donotremove=true

RUN apk add --no-cache curl docker-cli gettext jq zip

COPY swarmpages/lib/deploy.sh swarmpages/lib/utils.sh /usr/local/lib/
COPY swarmpages/swarmpages-build-docker.sh swarmpages/swarmpages-deploy.sh /usr/local/bin/
